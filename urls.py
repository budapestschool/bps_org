# -*- coding: utf-8 -*-
import os

import aldryn_addons.urls
from aldryn_django.utils import i18n_patterns
from django.conf.urls import include, url
from django.contrib.auth.views import redirect_to_login
from django.urls import reverse
from wagtailimportexport import urls as wagtailimportexport_urls

STAGE = os.environ.get("STAGE")


def redirect_to_my_auth(request):
    return redirect_to_login(reverse("wagtailadmin_home"), login_url="/login/auth0")


login_url = []
if STAGE != "local":
    # when we are on the test server or on the prod server
    # then we change to auth0 authentication
    login_url = [url(r"/admin/login", redirect_to_my_auth, name="wagtailadmin_login")]

urlpatterns = (
    login_url
    + [
        url(r"^", include(("django.contrib.auth.urls", "django"), namespace="auth")),
        url(r"^", include(("social_django.urls", "social_django"), namespace="social")),
        url(r"^i18n/", include("django.conf.urls.i18n")),
        url(r"", include(wagtailimportexport_urls)),
    ]
    + aldryn_addons.urls.patterns()
    + i18n_patterns(
        # add your own i18n patterns here
        *aldryn_addons.urls.i18n_patterns()  # MUST be the last entry!
    )
)
