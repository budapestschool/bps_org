from django.utils import translation
from wagtail.core.blocks import StructValue


class TranslatedField:
    def __init__(self, hu_field, en_field):
        self.hu_field = hu_field
        self.en_field = en_field

    def __get__(self, instance, owner):
        if translation.get_language() == "en":
            return getattr(instance, self.en_field)
        else:
            return getattr(instance, self.hu_field)


class I18NValue(StructValue):
    def proper_value(self, field):
        if translation.get_language() == "en":
            return self.get("{}_en".format(field))
        else:
            return self.get("{}_hu".format(field))

    def title(self):
        return self.proper_value("title")

    def content(self):
        return self.proper_value("content")

    def heading_text(self):
        return self.proper_value("heading_text")

    def text(self):
        return self.proper_value("text")

    def attribute_name(self):
        return self.proper_value("attribute_name")

    def description(self):
        return self.proper_value("description")

    def caption(self):
        return self.proper_value("caption")

    def lead(self):
        return self.proper_value("lead")

    def calling(self):
        return self.proper_value("calling")

    def button_text(self):
        return self.proper_value("button_text")

    def eyebrow(self):
        return self.proper_value("eyebrow")

    def form_id(self):
        return self.proper_value("form_id")
