from django.db import models
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.contrib.forms.models import AbstractForm, AbstractFormField
from wagtail.core.fields import RichTextField

from home.tasks import task_send_confirmation_email


class FormField(AbstractFormField):
    page = ParentalKey("FormPage", on_delete=models.CASCADE, related_name="form_fields")


class FormPage(AbstractForm):
    intro = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)
    notification_subject = models.CharField("Notification email subject", max_length=254)
    notification_email_text = RichTextField(blank=True)
    notificiation_from_email = models.CharField("notificaiton email from", max_length=100)

    content_panels = AbstractForm.content_panels + [
        FieldPanel("intro", classname="full"),
        InlinePanel("form_fields", label="Form fields"),
        FieldPanel("thank_you_text", classname="full"),
        MultiFieldPanel(
            [
                FieldPanel("notification_subject"),
                FieldPanel("notification_email_text", classname="full"),
                FieldPanel("notificiation_from_email"),
            ],
            "notification email",
        ),
    ]

    def process_form_submission(self, form):
        # first save the data as a usual wagtail form
        form_submission = super().process_form_submission(form)

        # now process the data in celery
        if "e-mail" in form.cleaned_data:
            email = form.cleaned_data["e-mail"]
            task_send_confirmation_email.delay(
                self.notification_subject,
                self.notification_email_text,
                self.notificiation_from_email,
                email,
            )

        return form_submission
