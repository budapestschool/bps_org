from __future__ import unicode_literals

from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, StreamFieldPanel
from wagtail.core import blocks
from wagtail.core.blocks import StructBlock
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from .page_meta_tags import PageMetaMixin

from home.i18util import I18NValue, TranslatedField
from home.blocks import MasonryBlock
from home.models.school import HeroMixin
from wagtail.core.blocks import (
    CharBlock,
    ChoiceBlock,
    ListBlock,
    PageChooserBlock,
    RichTextBlock,
    StreamBlock,
    StructBlock,
    TextBlock,
    URLBlock,
)

from .page_meta_tags import MenuTagLineMixin


class TilesMasonryPage(Page, MenuTagLineMixin, PageMetaMixin):
    title_en = models.CharField(max_length=255)
    translated_title = TranslatedField("title", "title_en")
    items = StreamField([("item", MasonryBlock())])

    content_panels = Page.content_panels + [FieldPanel("title_en"), StreamFieldPanel("items")]

    template = "home/tiles-masonry.html"

    def __str__(self):
        return self.translated_title

