from __future__ import unicode_literals

from wagtail.core.models import Page

from .page_meta_tags import PageMetaMixin
from .standard import StandardPage


class LandingPageIndex(Page):
    subpage_types = ["LandingPage", "StandardPage"]


class LandingPage(StandardPage, PageMetaMixin):
    parent_page_types = ["LandingPageIndex"]
    subpage_types = []

    promote_panels = StandardPage.promote_panels

    template = "home/standard_page.html"

    hide_site_navigation = True
