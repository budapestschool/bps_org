from __future__ import unicode_literals

from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from home.i18util import TranslatedField


"""
usually you want to set GA tracking code etc. for the whole site,
and sometimes you want to overwrite page by page. So you will find this as
settings (in util), and in landing page
"""


class PageMetaMixin(models.Model):

    ga_tracking_code = models.CharField(
        max_length=255, null=True, blank=True, help_text="GA tracking code"
    )

    og_title_hu = models.CharField(max_length=255, null=True, blank=True)
    og_title_en = models.CharField(max_length=255, null=True, blank=True)
    og_title = TranslatedField("og_title_hu", "og_title_en")

    og_description_hu = models.CharField(max_length=400, null=True, blank=True)
    og_description_en = models.CharField(max_length=400, null=True, blank=True)
    og_description = TranslatedField("og_description_hu", "og_description_en")

    og_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Image shared on facebook 1200 x 630 is the best",
    )

    facebook = models.URLField(help_text="Your Facebook page URL", null=True, blank=True)
    twitter = models.CharField(
        max_length=255, help_text="Your Twitter username, without the @", null=True, blank=True
    )

    promote_panels = [
        FieldPanel("ga_tracking_code"),
        MultiFieldPanel(
            [
                FieldPanel("og_title_hu", classname="title"),
                FieldPanel("og_title_en", classname="title"),
                FieldPanel("og_description_hu"),
                FieldPanel("og_description_en"),
                ImageChooserPanel("og_image"),
            ],
            heading="Open Graph settings",
        ),
        MultiFieldPanel([FieldPanel("facebook"), FieldPanel("twitter")]),
    ]

    class Meta:
        abstract = True


class MenuTagLineMixin(models.Model):
    menu_summary_hu = models.CharField(max_length=100, blank=True, null=True)
    menu_summary_en = models.CharField(max_length=100, blank=True, null=True)
    menu_summary = TranslatedField("menu_summary_hu", "menu_summary_en")

    promote_panels = [
        MultiFieldPanel(
            [FieldPanel("menu_summary_hu"), FieldPanel("menu_summary_en")],
            "Tagline about the page visible in the menu",
        )
    ]

    class Meta:
        abstract = True
