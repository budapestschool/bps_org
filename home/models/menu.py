from django.db import models
from django.utils.translation import ugettext_lazy as _
from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import FieldPanel, PageChooserPanel
from wagtailmenus.conf import settings
from wagtailmenus.models import AbstractFlatMenuItem

from home.i18util import TranslatedField


class TranslatedFlatMenuItem(AbstractFlatMenuItem):
    """A custom menu item model to be used by ``wagtailmenus.FlatMenu``"""

    menu = ParentalKey(
        "wagtailmenus.FlatMenu",
        on_delete=models.CASCADE,
        related_name="custom_menu_items",  # important for step 3!
    )

    link_text_en = models.CharField(
        verbose_name=_("link text EN"),
        max_length=255,
        blank=True,
        help_text=_(
            "Provide the text to use for a custom URL, or set on an internal "
            "page link to use instead of the page's title."
        ),
    )
    translated_link_text = TranslatedField("link_text", "link_text_en")

    @property
    def menu_text(self):
        """Use `translated_link_text` instead of just `link_text`"""
        return self.translated_link_text or getattr(
            self.link_page, app_settings.PAGE_FIELD_FOR_MENU_ITEM_TEXT, self.link_page.title
        )

    # Also override the panels attribute, so that the new fields appear
    # in the admin interface
    panels = [
        PageChooserPanel("link_page"),
        FieldPanel("link_url"),
        FieldPanel("url_append"),
        FieldPanel("link_text"),
        FieldPanel("link_text_en"),
        FieldPanel("allow_subnav"),
    ]
