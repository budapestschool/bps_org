from __future__ import unicode_literals

from django.db import models
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.admin.edit_handlers import (
    FieldPanel,
    FieldRowPanel,
    InlinePanel,
    MultiFieldPanel,
    StreamFieldPanel,
)
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Orderable, Page
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.search import index
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.snippets.models import register_snippet

from home.blocks import CMS_BLOCKS
from home.i18util import TranslatedField

from .hero import HeroMixin
from .page_meta_tags import PageMetaMixin
from .page_meta_tags import MenuTagLineMixin


@register_snippet
class Icon(ClusterableModel):
    name = models.CharField("Name", max_length=128)
    svg = models.TextField("Svg")
    panels = [FieldPanel("name"), FieldPanel("svg"), FieldPanel("svg")]

    search_fields = [index.SearchField("name")]


@register_snippet
class People(index.Indexed, ClusterableModel):
    """
    A Django model to store People objects.
    It uses the `@register_snippet` decorator to allow it to be accessible
    via the Snippets UI (e.g. /admin/snippets/base/people/)
    `People` uses the `ClusterableModel`, which allows the relationship with
    another model to be stored locally to the 'parent' model (e.g. a PageModel)
    until the parent is explicitly saved. This allows the editor to use the
    'Preview' button, to preview the content, without saving the relationships
    to the database.
    https://github.com/wagtail/django-modelcluster
    """

    first_name = models.CharField("First name", max_length=254)
    last_name = models.CharField("Last name", max_length=254)
    bio_hu = RichTextField("Bio")
    bio_en = RichTextField("TODO BIO")
    bio = TranslatedField("bio_hu", "bio_en")

    image = models.ForeignKey(
        "wagtailimages.Image", null=True, blank=True, on_delete=models.SET_NULL, related_name="+"
    )

    panels = [
        MultiFieldPanel(
            [
                FieldRowPanel(
                    [
                        FieldPanel("first_name", classname="col6"),
                        FieldPanel("last_name", classname="col6"),
                    ]
                )
            ],
            "Name",
        ),
        MultiFieldPanel([FieldPanel("bio_hu"), FieldPanel("bio_en")], "BIO"),
        ImageChooserPanel("image"),
    ]

    search_fields = [index.SearchField("first_name"), index.SearchField("last_name")]

    @property
    def thumb_image(self):
        # Returns an empty string if there is no profile pic or the rendition
        # file can't be found.
        try:
            return self.image.get_rendition("fill-50x50").img_tag()
        except Exception:
            return ""

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    class Meta:
        verbose_name = "Person"
        verbose_name_plural = "People"


class SchoolTeacherPlacement(Orderable, models.Model):
    school = ParentalKey(
        "home.SchoolPage", on_delete=models.CASCADE, related_name="teacher_placements"
    )
    teacher = models.ForeignKey("home.People", on_delete=models.CASCADE, related_name="+")

    title_hu = models.CharField("titulus", max_length=100, default="Tanár")
    title_en = models.CharField("title", max_length=100, default="Teacher")
    title = TranslatedField("title_hu", "title_en")

    class Meta:
        verbose_name = "teacher placement"
        verbose_name_plural = "teacher placements"

    panels = [
        SnippetChooserPanel("teacher"),
        MultiFieldPanel([FieldPanel("title_hu"), FieldPanel("title_en")], "Title"),
    ]

    def __str__(self):
        return self.school.title + " -> " + self.people


class SchoolPage(Page, HeroMixin, MenuTagLineMixin, PageMetaMixin):
    title_en = models.CharField(max_length=255)
    translated_title = TranslatedField("title", "title_en")

    body = StreamField(CMS_BLOCKS, verbose_name="Page body", blank=True)

    content_panels = (
        Page.content_panels
        + HeroMixin.content_panels
        + [
            FieldPanel("title_en", classname="title"),
            StreamFieldPanel("body"),
            InlinePanel("teacher_placements", label="Teachers"),
        ]
    )

    promote_panels = Page.promote_panels + MenuTagLineMixin.promote_panels + PageMetaMixin.promote_panels
