from __future__ import unicode_literals

from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, FieldRowPanel, MultiFieldPanel, PageChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel

from home.i18util import TranslatedField


class HeroMixin(models.Model):

    hero_slideshow = models.ForeignKey(
        "home.SlideShowPage",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Use this if you want a carousel on the cover",
    )

    # Hero section of HomePage
    hero_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Cover image 1600x500 if you want to use a single one",
    )

    hero_text_title_hu = models.CharField(
        max_length=255, null=True, blank=True, help_text="Hero cim ide"
    )

    hero_text_title_en = models.CharField(
        max_length=255, null=True, blank=True, help_text="Hero title"
    )

    hero_text_title = TranslatedField("hero_text_title_hu", "hero_text_title_en")

    hero_text_subtitle_hu = models.CharField(
        max_length=255, null=True, blank=True, help_text="Hero cim ide"
    )

    hero_text_subtitle_en = models.CharField(
        max_length=255, null=True, blank=True, help_text="Hero title"
    )

    hero_text_subtitle = TranslatedField("hero_text_subtitle_hu", "hero_text_subtitle_en")

    hero_cta_hu = models.CharField(
        verbose_name="Hero Gomb",
        max_length=255,
        null=True,
        blank=True,
        help_text="CTO gomb szovege",
    )
    hero_cta_en = models.CharField(
        verbose_name="Hero CTA",
        max_length=255,
        null=True,
        blank=True,
        help_text="Text to display on Call to Action",
    )

    hero_cta = TranslatedField("hero_cta_hu", "hero_cta_en")

    hero_cta_link = models.CharField(
        verbose_name="CTA link", max_length=512, null=True, blank=True, help_text="The link"
    )

    content_panels = [
        MultiFieldPanel(
            [
                PageChooserPanel("hero_slideshow"),
                MultiFieldPanel(
                    [
                        ImageChooserPanel("hero_image"),
                        FieldRowPanel(
                            [
                                FieldPanel("hero_text_title_hu", classname="title"),
                                FieldPanel("hero_text_title_en", classname="title"),
                            ],
                            heading="Hero section title",
                        ),
                        FieldRowPanel(
                            [
                                FieldPanel("hero_text_subtitle_hu", classname="full"),
                                FieldPanel("hero_text_subtitle_en", classname="full"),
                            ],
                            heading="Hero section subtitle",
                        ),
                    ],
                    heading="single cover image",
                    classname="collapsible",
                ),
                MultiFieldPanel(
                    [
                        FieldPanel("hero_cta_hu"),
                        FieldPanel("hero_cta_en"),
                        FieldPanel("hero_cta_link"),
                    ],
                    heading="Call for action",
                ),
            ],
            heading="Hero section",
            classname="collapsible collapsed",
        )
    ]

    class Meta:
        abstract = True
