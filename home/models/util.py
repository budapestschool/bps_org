from __future__ import absolute_import, unicode_literals

from django.db import models
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from wagtail.admin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel, PageChooserPanel
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.core.models import Orderable

from .page_meta_tags import PageMetaMixin

# The LinkFields and RelatedLink meta-models are taken from the WagtailDemo implementation.
# They provide a multi-field panel that allows you to set a link title and choose either
# an internal or external link. It also provides a custom property ('link') to simplify
# using it in the template.


class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        "wagtailcore.Page", null=True, blank=True, on_delete=models.SET_NULL, related_name="+"
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        else:
            return self.link_external

    panels = [FieldPanel("link_external"), PageChooserPanel("link_page")]

    class Meta:
        abstract = True


class RelatedLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")

    panels = [FieldPanel("title"), MultiFieldPanel(LinkFields.panels, "Link")]

    class Meta:
        abstract = True


# The SocialMediaSettings model provides site-specific social media links.
# These could be easily expanded to include any number of social media URLs / IDs.


@register_setting
class SocialMediaSettings(BaseSetting, PageMetaMixin):
    panels = PageMetaMixin.promote_panels


@register_setting
class I18NSettings(BaseSetting):
    language_chooser = models.NullBooleanField(
        null=True, blank=True, help_text="shall we render the language chooser menu item?"
    )


# The FooterLinks model takes advantage of the RelatedLink model we implemented above.


@register_setting
class FooterLinks(BaseSetting, ClusterableModel):

    panels = [InlinePanel("footer_links", label="Footer Links")]


class FooterLinksRelatedLink(Orderable, RelatedLink):
    page = ParentalKey("FooterLinks", related_name="footer_links")
