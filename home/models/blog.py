from __future__ import unicode_literals

from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, StreamFieldPanel
from wagtail.core import blocks
from wagtail.core.blocks import StructBlock
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel
from .page_meta_tags import PageMetaMixin
from home.blocks import CMS_BLOCKS, BLOG_BLOCKS, HasLeadMixin
from home.i18util import I18NValue, TranslatedField
from home.models.school import HeroMixin

from .page_meta_tags import MenuTagLineMixin


class BlogPage(Page, HeroMixin, MenuTagLineMixin, PageMetaMixin):
    title_en = models.CharField(max_length=255)

    translated_title = TranslatedField("title", "title_en")

    lead_hu = RichTextField(blank=True)
    lead_en = RichTextField(blank=True)
    lead = TranslatedField("lead_hu", "lead_en")
    unlisted = models.BooleanField(default=False, blank=False)

    thumbnail = models.ForeignKey(
        "wagtailimages.Image", null=True, blank=False, on_delete=models.SET_NULL, related_name="+"
    )
    body = StreamField(BLOG_BLOCKS, verbose_name="Page body", blank=False)

    promote_panels = [
        FieldPanel("unlisted"),
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
    ] + PageMetaMixin.promote_panels

    content_panels = (
        Page.content_panels
        + HeroMixin.content_panels
        + [
            FieldPanel("title_en"),
            ImageChooserPanel("thumbnail"),
            FieldPanel("lead_hu"),
            FieldPanel("lead_en", classname="full"),
            StreamFieldPanel("body"),
        ]
    )

    def get_blog(self):
        return self.get_ancestors().type(Blog).last().blog

    def latest_posts(self):
        return self.get_blog().latest_posts()

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["blog"] = self.get_blog()
        return context

    def __str__(self):
        return self.translated_title

    parent_page_types = ["Blog"]


class Blog(Page):
    subpage_types = ["BlogPage"]

    def child_pages(self):
        return (
            BlogPage.objects.live()
            .filter(unlisted=False)
            .child_of(self)
            .order_by("-latest_revision_created_at")
        )

    def latest_posts(self):
        return (
            BlogPage.objects.live()
            .filter(unlisted=False)
            .child_of(self)
            .order_by("-latest_revision_created_at")[:3]
        )
