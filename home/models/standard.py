from __future__ import unicode_literals

from django.db import models
from wagtail.admin.edit_handlers import FieldPanel, MultiFieldPanel, StreamFieldPanel
from wagtail.core import blocks
from wagtail.core.blocks import StructBlock
from wagtail.core.fields import RichTextField, StreamField
from wagtail.core.models import Page
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.edit_handlers import ImageChooserPanel

from home.blocks import CMS_BLOCKS, BLOG_BLOCKS, HasLeadMixin
from home.i18util import I18NValue, TranslatedField
from home.models.school import HeroMixin
from .page_meta_tags import PageMetaMixin
from .page_meta_tags import MenuTagLineMixin


class HomePage(Page):
    backgroung_image = models.ForeignKey(
        "wagtailimages.Image", null=True, blank=True, on_delete=models.SET_NULL, related_name="+"
    )

    title_en = models.CharField(max_length=255)

    translated_title = TranslatedField("title", "title_en")

    body_hu = RichTextField(blank=True)
    body_en = RichTextField(blank=True)
    body = TranslatedField("body_hu", "body_en")

    content_panels = Page.content_panels + [
        FieldPanel("title_en", classname="title"),
        FieldPanel("body_hu", classname="full"),
        FieldPanel("body_en", classname="full"),
        ImageChooserPanel("backgroung_image"),
    ]


class StandardPage(Page, HeroMixin, MenuTagLineMixin, PageMetaMixin):
    title_en = models.CharField(max_length=255)

    translated_title = TranslatedField("title", "title_en")

    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Landscape mode only; horizontal width between 1000px and 3000px.",
    )
    body = StreamField(CMS_BLOCKS, verbose_name="Page body", blank=True)

    big_text_style = models.BooleanField(help_text="check if the page is text heavy", default=False)

    hide_site_navigation = models.BooleanField(
        default=False, help_text="hide site navigation (for cover and landing pages)"
    )

    content_panels = (
        Page.content_panels
        + HeroMixin.content_panels
        + [
            FieldPanel("title_en", classname="title"),
            StreamFieldPanel("body"),
            ImageChooserPanel("image"),
        ]
    )

    promote_panels = [
        MultiFieldPanel(Page.promote_panels, "Common page configuration"),
        FieldPanel("big_text_style"),
        FieldPanel("hide_site_navigation"),
    ] + MenuTagLineMixin.promote_panels + PageMetaMixin.promote_panels

    def __str__(self):
        return self.translated_title



class ImageSlideBlock(StructBlock):
    image = ImageChooserBlock(required=False)
    caption_hu = blocks.CharBlock()
    caption_en = blocks.CharBlock()

    class Meta:
        icon = ("fa-image",)
        value_class = I18NValue


class SlideShowPage(Page):
    title_en = models.CharField(max_length=255)

    translated_title = TranslatedField("title", "title_en")

    slides = StreamField([("image", ImageSlideBlock())])

    content_panels = Page.content_panels + [
        FieldPanel("title_en", classname="title"),
        StreamFieldPanel("slides"),
    ]
