from django.shortcuts import redirect
from wagtail.core import hooks
from wagtail.core.models import PageViewRestriction


@hooks.register("before_serve_page", order=-1)
def check_view_restrictions_and_redirect_to_error_page(page, request, serve_args, serve_kwargs):
    for restriction in page.get_view_restrictions():
        if not restriction.accept_request(request):
            if restriction.restriction_type in [
                PageViewRestriction.LOGIN,
                PageViewRestriction.GROUPS,
            ]:
                return redirect("/no-permissions")
