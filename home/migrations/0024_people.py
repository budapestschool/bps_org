# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-11-10 01:31
from __future__ import unicode_literals

import django.db.models.deletion
import wagtail.search.index
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("wagtailimages", "0019_delete_filter"), ("home", "0023_schoolpage")]

    operations = [
        migrations.CreateModel(
            name="People",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
                    ),
                ),
                ("first_name", models.CharField(max_length=254, verbose_name="First name")),
                ("last_name", models.CharField(max_length=254, verbose_name="Last name")),
                (
                    "image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.Image",
                    ),
                ),
            ],
            options={"verbose_name": "Person", "verbose_name_plural": "People"},
            bases=(wagtail.search.index.Indexed, models.Model),
        )
    ]
