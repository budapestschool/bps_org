# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-11-25 09:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("home", "0054_auto_20181123_0944")]

    operations = [
        migrations.RemoveField(model_name="standardpage", name="introduction"),
        migrations.AddField(
            model_name="standardpage",
            name="big_text_style",
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
