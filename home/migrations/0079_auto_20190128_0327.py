# -*- coding: utf-8 -*-
# Generated by Django 1.11.18 on 2019-01-28 03:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [("home", "0078_auto_20190128_0324")]

    operations = [
        migrations.AlterField(
            model_name="landingpage",
            name="og_description_en",
            field=models.CharField(blank=True, max_length=400, null=True),
        ),
        migrations.AlterField(
            model_name="landingpage",
            name="og_description_hu",
            field=models.CharField(blank=True, max_length=400, null=True),
        ),
        migrations.AlterField(
            model_name="landingpage",
            name="og_title_en",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="landingpage",
            name="og_title_hu",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="socialmediasettings",
            name="og_description_en",
            field=models.CharField(blank=True, max_length=400, null=True),
        ),
        migrations.AlterField(
            model_name="socialmediasettings",
            name="og_description_hu",
            field=models.CharField(blank=True, max_length=400, null=True),
        ),
        migrations.AlterField(
            model_name="socialmediasettings",
            name="og_title_en",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name="socialmediasettings",
            name="og_title_hu",
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
