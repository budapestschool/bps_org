# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2018-11-21 07:33
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [("home", "0041_auto_20181121_0633")]

    operations = [
        migrations.RenameField(
            model_name="sitebranding", old_name="site_logo", new_name="site_logo_dark"
        ),
        migrations.RemoveField(model_name="sitebranding", name="banner_colour"),
    ]
