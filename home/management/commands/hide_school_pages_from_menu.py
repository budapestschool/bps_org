import itertools
import logging

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from wagtail.core.models import BaseViewRestriction, Page, PageViewRestriction, Site

from home.models import  SchoolPage, StandardPage

logger = logging.getLogger(__name__)



class Command(BaseCommand):
    def handle(self, *args, **options):
        helyszineink = StandardPage.objects.get(title="Helyszíneink")
        schools = helyszineink.get_children().type(SchoolPage)
        for s in schools:
            print(s.title)
            s.show_in_menus = False
            s.save()

