import os
import json
import zipfile
from django.core.management.base import BaseCommand
from django.core.files.storage import FileSystemStorage
from bps_org.home.models.school import People

class Command(BaseCommand):
    help = 'Export People objects and associated media files'

    def handle(self, *args, **options):
        people_data = []
        image_storage = FileSystemStorage()

        for person in People.objects.all():
            person_info = {
                'first_name': person.first_name,
                'last_name': person.last_name,
                'bio': person.bio,
                # Add more fields as needed
            }

            if person.image:
                image_filename = os.path.basename(person.image.file.name)
                image_path = f'exported_images/{image_filename}'
                image_storage.save(image_path, person.image.file)
                person_info['image_path'] = image_path

            people_data.append(person_info)

        with open('exported_people_data.json', 'w') as file:
            json.dump(people_data, file)

        # Create a ZIP file of the exported images
        zip_filename = 'exported_images.zip'
        with zipfile.ZipFile(zip_filename, 'w') as zip_file:
            for root, _, files in os.walk('exported_images'):
                for file in files:
                    file_path = os.path.join(root, file)
                    zip_file.write(file_path, os.path.relpath(file_path, 'exported_images'))

        self.stdout.write(self.style.SUCCESS('People data exported successfully.'))
