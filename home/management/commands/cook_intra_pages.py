import itertools
import logging

from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from wagtail.core.models import BaseViewRestriction, Page, PageViewRestriction, Site

from home.models import StandardPage

logger = logging.getLogger(__name__)
schools = ["adalovelace", "b38", "romai", "brezno", "p10", "solymar", "campona", "bps", "lab"]
roles = ["teacher", "operator", "parent"]


class IntraPageManager:
    def __init__(self):
        try:
            self.site = Site.objects.get(hostname="our.budapestschool.org")
        except Site.DoesNotExist:
            raise CommandError("our.budapestschool.org site does not exist in the db")

        self.intra_page = self.site.root_page

    def cook_groups(self):
        # this makes sure that we have the right groups in the database
        for (team_name, role) in itertools.product(schools, roles):
            group_name = "{}_{}".format(team_name.lower(), role)
            new_group, created = Group.objects.get_or_create(name=group_name)
            if created:
                logging.info("new group created as {}".format(new_group.name))
            else:
                logging.info("{} group was ok".format(new_group.name))

    def create_or_get_intra_page(self, title):
        # check if we have the page for all parents
        result = Page.objects.live().child_of(self.site.root_page).filter(title=title)

        page = None
        if len(result) == 0:
            logging.info("can't find the page {}. Creating one. ".format(title))

            page = StandardPage(title=title, title_en=title)
            self.intra_page.add_child(instance=page)
            page.save()

        elif len(result) > 1:
            raise CommandError("there is more pages on intranet with the title {}").format(title)
        else:
            logging.info("'{}' page found.".format(title))
            page = result[0]

        return page

    def setPermissions(self, page, group_names):
        # and now we give access to all parents, teachers and operators
        pvr, created = PageViewRestriction.objects.get_or_create(page=page)
        if created:
            logging.info("new PageViewRestriction created for {}".format(page.title))
        else:
            logging.info("found a PageViewRestriction object for {}".format(page.title))

        pvr.restriction_type = BaseViewRestriction.GROUPS
        logging.info("make sure {} have access to {}".format(group_names, page.title))
        groups = [Group.objects.get(name=group_name) for group_name in group_names]
        pvr.groups = groups
        pvr.save()
        return pvr

    def create_school_pages(self, school):
        """ creates the page and set it's permissions """
        if school not in school:
            raise CommandError("don't know anything about the school: {}").format(school)

        # everybody has access to parents' page
        parents_title = "{} parents".format(school)
        parents_page = self.create_or_get_intra_page(parents_title)
        group_names = ["{}_{}".format(school, role) for role in roles]
        self.setPermissions(parents_page, group_names)

        # only teachers and operators have access to teachers' page
        page_title = "{} teachers".format(school)
        parents_page = self.create_or_get_intra_page(page_title)
        group_names = ["{}_{}".format(school, role) for role in ["teacher", "operator"]]
        self.setPermissions(parents_page, group_names)


class Command(BaseCommand):
    def handle(self, *args, **options):
        ipm = IntraPageManager()
        ipm.cook_groups()
        for school_name in schools:
            ipm.create_school_pages(school_name)
