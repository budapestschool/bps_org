from django.core.management.base import BaseCommand

from home.models import SchoolPage


class Command(BaseCommand):
    help = "Export content to be translated"

    def handle(self, *args, **options):
        for school in SchoolPage.objects.all():
            for cucc in school.body:
                print(str(cucc.render_as_block()))
