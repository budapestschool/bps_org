from typing import Text
from wagtail.core.blocks import (
    CharBlock,
    ChoiceBlock,
    ListBlock,
    PageChooserBlock,
    RichTextBlock,
    StreamBlock,
    StructBlock,
    TextBlock,
    URLBlock,
)
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.blocks import ImageChooserBlock
from wagtail.snippets.blocks import SnippetChooserBlock

from home.i18util import I18NValue


""" just use text, content, caption etc fields with both languages, and add
    value_class = I18NValue
"""


class ImageBlock(StructBlock):
    """
    Custom `StructBlock` for utilizing images with associated caption and
    attribution data
    """

    image = ImageChooserBlock(required=True)
    caption_hu = CharBlock(required=False)
    caption_en = CharBlock(required=False)
    attribution = CharBlock(required=False)

    class Meta:
        icon = "image"
        template = "blocks/image_block.html"
        value_class = I18NValue


class HasLeadMixin(StructBlock):
    lead_hu = RichTextBlock(required=False, features=["bold", "italic", "link"])
    lead_en = RichTextBlock(required=False, features=["bold", "italic", "link"])


class LinkableMixin(StructBlock):
    link_id = CharBlock(help_text="For making hyperlinks to this block", required=False)


class HeadingBlock(LinkableMixin, HasLeadMixin, StructBlock):
    """
    Custom `StructBlock` that allows the user to select h2 - h4 sizes for headers
    """

    heading_text_hu = CharBlock(classname="title", required=True)
    heading_text_en = CharBlock(classname="title", required=True)
    size = ChoiceBlock(
        choices=[("", "Select a header size"), ("h2", "H2"), ("h3", "H3"), ("h4", "H4")],
        blank=True,
        required=False,
    )

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context=parent_context)
        context["is_featured"] = value.get("size") == "h2" or value.get("size") == "h3"
        return context

    class Meta:
        icon = "title"
        template = "blocks/heading_block.html"
        value_class = I18NValue


class BlockQuote(StructBlock):
    """
    Custom `StructBlock` that allows the user to attribute a quote to the author
    """

    text_hu = TextBlock()
    text_en = TextBlock()
    attribute_name_hu = CharBlock(blank=True, required=False, label="e.g. Berry Mária")

    attribute_name_en = CharBlock(blank=True, required=False, label="e.g. Mary Berry")

    title_hu = CharBlock(blank=True, required=False, label="cím")

    title_en = CharBlock(blank=True, required=False, label="title")

    image = ImageChooserBlock(required=False, label="Profile Pic (square)")

    class Meta:
        icon = "quote-left"
        template = "blocks/blockquote.html"
        value_class = I18NValue


class CTABlock(StructBlock):
    style = ChoiceBlock(
        choices=[("bordered", "Bordered"), ("minimal", "Minimal")],
        required=False,
        help_text="minimal nem hasznal ikont",
    )

    icon = SnippetChooserBlock("home.Icon", required=False, help_text="elhagyhato")
    calling_hu = CharBlock(classname="title", required=True)
    calling_en = CharBlock(classname="title", required=True)
    description_hu = TextBlock()
    description_en = TextBlock()
    url = URLBlock()
    button_text_hu = CharBlock(required=True)
    button_text_en = CharBlock(required=True)

    class Meta:
        template = "blocks/cta_block.html"
        icon = "ad"
        value_class = I18NValue


class VideoPresentationBlock(StructBlock):
    eyebrow_hu = CharBlock()
    eyebrow_en = CharBlock()
    title_hu = CharBlock(classname="title", required=True)
    title_en = CharBlock(classname="title", required=True)
    description_hu = TextBlock()
    description_en = TextBlock()
    video_url = URLBlock()

    class Meta:
        template = "blocks/video_presentation_block.html"
        icon = "media"
        value_class = I18NValue


class Accordions(StructBlock):
    style = ChoiceBlock(
        choices=[("default", "Default"), ("minimal", "Minimal"), ("steps", "Steps")]
    )
    tabs = ListBlock(
        StructBlock(
            [
                ("title_hu", CharBlock()),
                ("title_en", CharBlock()),
                ("content_hu", RichTextBlock(required=False)),
                ("content_en", RichTextBlock(required=False)),
            ],
            value_class=I18NValue,
        )
    )

    class Meta:
        icon = "bars"
        template = "blocks/accordions.html"
        value_class = I18NValue


class MasonryBlock(StructBlock):
    eyebrow_hu = CharBlock()
    eyebrow_en = CharBlock()
    title_hu = CharBlock(classname="title", required=True)
    title_en = CharBlock(classname="title", required=True)
    link = PageChooserBlock()
    image = ImageChooserBlock()

    class Meta:
        template = "blocks/tile_masonry_block.html"
        icon = "media"
        value_class = I18NValue


class Masonry(StructBlock):

    items = ListBlock(MasonryBlock)

    class Meta:
        icon = "columns"
        template = "blocks/masonry.html"
        value_class = I18NValue


class Features(StructBlock):

    features = ListBlock(
        StructBlock(
            [
                ("image", ImageChooserBlock(required=False, label="400x200")),
                ("title_hu", CharBlock()),
                ("title_en", CharBlock()),
                ("content_hu", RichTextBlock(required=False)),
                ("content_en", RichTextBlock(required=False)),
                ("more", PageChooserBlock(required=False)),
            ],
            value_class=I18NValue,
        )
    )

    class Meta:
        icon = "columns"
        template = "blocks/features.html"
        value_class = I18NValue


class BilingualText(StructBlock):
    text_hu = RichTextBlock()
    text_en = RichTextBlock()

    class Meta:
        icon = ("paragraph",)
        template = "blocks/text.html"
        value_class = I18NValue


class TypeFormEmbed(StructBlock):
    form_id_hu = TextBlock(
        help_text="A typeform ID-ja, amit az URL-ből tudsz kimásolni. Például a https://hp6.typeform.com/to/SRgf2p0F címből a SRgf2p0F rész."
    )
    form_id_en = TextBlock(help_text="ID of the typeform")
    hidden_name = TextBlock(help_text="hidden field name", required=False)
    hidden_value = TextBlock(help_text="hidden field value", required=False)

    class Meta:
        icon = "fa-exclamation-triangle"
        template = "blocks/typeform.html"
        value_class = I18NValue


BASE_BLOCKS = [
    ("heading_block", HeadingBlock()),
    ("text_block", BilingualText()),
    ("image_block", ImageBlock()),
    ("cta_block", CTABlock()),
    ("video_presenation", VideoPresentationBlock()),
    ("accordions_block", Accordions()),
    ("features", Features()),
    ("masonry", Masonry()),
    ("block_quote", BlockQuote()),
    ("typeform_block", TypeFormEmbed()),
    (
        "slide_show",
        PageChooserBlock(target_model="home.SlideShowPage", template="blocks/slideshow_block.html"),
    ),
    (
        "embed_block",
        EmbedBlock(
            help_text="Insert an embed URL e.g https://www.youtube.com/embed/SGJFWirQ3ks",
            icon="fa-s15",
            template="blocks/embed_block.html",
        ),
    ),
]

BLOG_BLOCKS = [
    ("text_block", BilingualText()),
    ("image_block", ImageBlock()),
    ("video_presenation", VideoPresentationBlock()),
    ("block_quote", BlockQuote()),
    (
        "slide_show",
        PageChooserBlock(target_model="home.SlideShowPage", template="blocks/slideshow_block.html"),
    ),
]


class ColumnsBlock(StructBlock):
    left_column = StreamBlock(BASE_BLOCKS)
    right_column = StreamBlock(BASE_BLOCKS)

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context)
        context["left_column"] = value.get("left_column")
        context["right_column"] = value.get("right_column")
        return context

    class Meta:
        icon = "fa fa-columns"
        label = "Columns 1-1"
        template = None


class Columns1To1Block(ColumnsBlock):
    class Meta:
        label = "Columns 1:1"
        template = "blocks/columns-1-1.html"


class Columns1To2Block(ColumnsBlock):
    class Meta:
        label = "Columns 1:2"
        template = "blocks/columns-1-2.html"


class Columns2To1Block(ColumnsBlock):
    class Meta:
        label = "Columns 2:1"
        template = "blocks/columns-2-1.html"


class Columns1To1To1Block(ColumnsBlock):
    middle_column = StreamBlock(BASE_BLOCKS)

    class Meta:
        label = "Columns 1:1:1"
        template = "blocks/columns-1-1-1.html"

    def get_context(self, value, parent_context=None):
        context = super().get_context(value, parent_context)
        context["middle_column"] = value.get("middle_column")
        return context


COLUMNS_BLOCKS = [
    ("columns_1_to_1", Columns1To1Block()),
    ("columns_1_to_2", Columns1To2Block()),
    ("columns_2_to_1", Columns2To1Block()),
    ("columns_1_to_1_to_1", Columns1To1To1Block()),
]

CMS_BLOCKS = BASE_BLOCKS + COLUMNS_BLOCKS
