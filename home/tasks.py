from celery import shared_task
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.core.mail.backends.smtp import EmailBackend
from django.utils.html import strip_tags


@shared_task
def task_send_confirmation_email(subject, body, from_email, to):
    backend = EmailBackend(
        host=settings.SMTP_SERVER,
        port=settings.SMTP_PORT,
        username=settings.SMTP_USERNAME,
        password=settings.SMTP_PASSWORD,
        use_tls=True,
        fail_silently=False,
    )
    # this is the source of knowledge:
    # https://stackoverflow.com/questions/3005080/how-to-send-html-email-with-django-with-dynamic-content-in-it
    # create the email, and attach the HTML version as well.
    html_content = body
    text_content = strip_tags(html_content)
    email = EmailMultiAlternatives(
        subject=subject, body=text_content, from_email=from_email, to=[to], connection=backend
    )
    email.attach_alternative(html_content, "text/html")
    email.send()
