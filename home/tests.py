from wagtail.core.models import Page, Site
from wagtail.tests.utils import WagtailPageTests

from home.models import LandingPage
from home.models import Blog, BlogPage


class MyPageTests(WagtailPageTests):
    def setUp(self):
        self.login()
        self.site = Site.objects.get()
        self.root = Page.get_root_nodes()[0]  # Home
        self.page = LandingPage(title="Leszallo oldal", title_en="Lading page")
        self.root.add_child(instance=self.page)
        self.site.root_page = self.page
        self.page.save()
        self.site.save()

    def test_landing_is_available(self):
        response = self.client.get("/home/")
        self.assertEqual(response.status_code, 200)


def BlogTests(WagtailPageTests):
    def addBlogPage(self, title):
        blogPost = BlogPage(title=title, title_en="en" + title)
        self.blog.add_child(instance=blogPost)
        self.blog.save()
        blogPost.save()
        return blogPost

    def setUp(self):
        self.site = Site.objects.get()
        self.root = Page.get_root_nodes()[0]  # Home
        self.blog = Blog(title="blog", title_en="blog", slug="home")
        self.root.add_child(instance=self.blog)
        self.site.root_page = self.blog
        self.blog.save()
        self.site.save()

        self.post1 = self.addBlogPage("elso blog")
        self.post2 = self.addBlogPage("masodik blog")
        self.post3 = self.addBlogPage("harmadik blog")

    def testLatestPosts(self):
        post4 = self.addBlogPage("negyedik")
        posts = self.blog.latest_blog_posts()
        self.assertEqual(posts[0].title, "negyedik")
