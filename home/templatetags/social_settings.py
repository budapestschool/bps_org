from django import template

from home.models.page_meta_tags import PageMetaMixin
from home.models.util import SocialMediaSettings
from django.utils import translation
register = template.Library()

"""
    social media attributes can be set for the site and can be overwritten
    by the current page. This template tag helps you to get a personalized
    value. In the template simple write
    <socail_settings ga_tracking_code/>

    if the page defines ga_trackind_code then you get it. If not, then you
    get the one defined in settings for the site.
"""


def get_page_or_site_attr(name, page, site):
    value = None
    if page:
        value = getattr(page, name, None)

    if value is None or value == "":
        value = getattr(site, name)

    return value


@register.simple_tag(takes_context=True)
def retrieve_social_settings(context):
    page = context["page"]
    request = context["request"]
    site_social_media_settings = SocialMediaSettings.for_site(request.site)

    social_settings = {}

    for f in PageMetaMixin._meta.get_fields():
        name = f.name
        value = get_page_or_site_attr(name, page, site_social_media_settings)

        social_settings[name] = value

    translated_title = social_settings["og_title_hu"]
    translated_description = social_settings["og_description_hu"]

    if translation.get_language() == "en":
        translated_title = social_settings["og_title_en"]
        translated_description = social_settings["og_description_en"]

    social_settings["og_title"] = translated_title
    social_settings["og_description"] = translated_description

    context["social_settings"] = social_settings
    return ""
