# import the logging library
import logging
import re

from django.contrib.auth.models import Group

from .bps_api import bps_client

logger = logging.getLogger(__name__)


def make_bps_users_admin(backend, user, response, *args, **kwargs):
    auth0user = user.social_auth.get(provider="auth0")
    if re.match(r"google-apps|[^@]+@budapestschool.org", auth0user.uid):
        logger.info("user %s set to staff by email" % auth0user.uid)
        user.is_staff = True
        user.is_superuser = True
        user.save()
    else:
        logger.info("user %s logged in without staff access " % auth0user.uid)


def sync_users_with_bps(backend, user, response, *args, **kwargs):
    auth0user = user.social_auth.get(provider="auth0")

    if not user.email:
        # Try to fill email for already existing users in the database
        details = kwargs.get("details", {})
        if details.get("email"):
            user.email = details.get("email")
            user.first_name = details.get("first_name")
            user.save()
        else:
            logger.info("User {} doesn't have an email. Can't sync with BPS.".format(auth0user.uid))
            return

    response = bps_client.post(
        "/auth/sync/",
        {"auth0_id": auth0user.uid, "email": user.email, "first_name": user.first_name},
    ).json()

    if "groups" in response:
        for group_id in response["groups"]:
            group = Group.objects.filter(name=group_id).first()
            if group:
                group.user_set.add(user)
                logger.info("User {} added to a {} group.".format(user.email, group_id))
            else:
                logger.info("Group {} doesn't exist.".format(group_id))
