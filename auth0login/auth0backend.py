# auth0login/auth0backend.py
import logging

import requests
from django.shortcuts import redirect
from social_core.backends.oauth import BaseOAuth2
from social_core.exceptions import AuthCanceled, AuthFailed

logger = logging.getLogger(__name__)


class Auth0(BaseOAuth2):
    """Auth0 OAuth authentication backend"""

    name = "auth0"
    SCOPE_SEPARATOR = " "
    ACCESS_TOKEN_METHOD = "POST"
    EXTRA_DATA = [("picture", "picture")]

    def authorization_url(self):
        """Return the authorization endpoint."""
        return "https://" + self.setting("DOMAIN") + "/authorize"

    def access_token_url(self):
        """Return the token endpoint."""
        return "https://" + self.setting("DOMAIN") + "/oauth/token"

    def get_user_id(self, details, response):
        """Return current user id."""
        return details["user_id"]

    def auth_complete(self, *args, **kwargs):
        try:
            return super().auth_complete(*args, **kwargs)
        except (AuthFailed, AuthCanceled) as error:
            logger.info("Processing authentication error: {}".format(error))

            if "Please verify your email before logging in" in str(error):
                return redirect("/verify-email")

            raise error

    def get_user_details(self, response):
        url = "https://" + self.setting("DOMAIN") + "/userinfo"
        headers = {"authorization": "Bearer " + response["access_token"]}
        resp = requests.get(url, headers=headers)
        userinfo = resp.json()

        logger.info("Userinfo from auth0: {}".format(userinfo))

        return {
            "username": userinfo["nickname"],
            "email": userinfo["email"],
            "first_name": userinfo["name"],
            "picture": userinfo["picture"],
            "user_id": userinfo["sub"],
        }
