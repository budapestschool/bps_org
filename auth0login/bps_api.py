import logging
from calendar import timegm
from datetime import timedelta

import requests
from django.conf import settings
from django.utils.timezone import now

logger = logging.getLogger(__name__)


class BpsAPIClient:
    def __init__(self, api_root):
        self.api_root = api_root
        self.generate_new_token()

    def generate_new_token(self):
        url = "https://{}/oauth/token".format(settings.SOCIAL_AUTH_AUTH0_DOMAIN)
        response = requests.post(
            url,
            {
                "client_id": settings.SOCIAL_AUTH_AUTH0_KEY,
                "client_secret": settings.SOCIAL_AUTH_AUTH0_SECRET,
                "audience": "https://users.budapestschool.org/api/v1/",
                "grant_type": "client_credentials",
            },
        ).json()

        self.api_token = response["access_token"]
        self.expires_at = timegm((now() + timedelta(seconds=response["expires_in"])).utctimetuple())

        logger.info("Generated new bps token {}".format(response["expires_in"]))

        return response

    def is_token_expired(self):
        return self.expires_at < timegm(now().utctimetuple()) + 5

    def check_token(self):
        if self.is_token_expired():
            self.generate_new_token()

    def get(self, url):
        self.check_token()
        return requests.get(
            "{}{}".format(self.api_root, url),
            headers={"Authorization": "Bearer {}".format(self.api_token)},
        )

    def post(self, url, data):
        self.check_token()
        return requests.post(
            "{}{}".format(self.api_root, url),
            data,
            headers={"Authorization": "Bearer {}".format(self.api_token)},
        )


bps_client = BpsAPIClient(settings.BPS_API_ROOT)
