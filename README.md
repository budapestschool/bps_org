# BPS website

This is a Divio managed Django/Wagtail website.

The site is using 4 docker containers:

1. django web
2. postgres db
3. celery worker (for email sending)
4. rabbitmq

## Developer environment set up

- Install Divio CLI tools: `pip install divio-cli`
- Check that you have all the required dependencies: `divio doctor`
- Pull the repository and set up the docker images: `divio project setup bps-site`
- Pull project files: `divio project pull db` and optionally `divio project pull media`
- Sync project: `divio project update`
- Start the project: `divio project up`

## Auto asset (CSS/JS) generation

- `docker-compose exec web gulp watch`

## Changing the model

- `docker-compose exec web python manage.py makemigrations`
- `docker-compose exec web python manage.py migrate`

## Testing production links locally (OSX)

The production content contains hard-coded links to subpages with the production domain. This makes it hard to explore the website locally and see your changes.

1. Map budapestschool.org to localhost in your /etc/hosts: `sudo echo "127.0.0.1 budapestschool.org" >> /etc/hosts`
2. [Port forward](https://salferrarello.com/mac-pfctl-port-forwarding/) 80 to 8000: `echo "rdr pass inet proto tcp from any to any port 80 -> 127.0.0.1 port 8000" | sudo pfctl -ef -`
