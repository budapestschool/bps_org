# -*- coding: utf-8 -*-
import os

import aldryn_addons.settings

INSTALLED_ADDONS = [
    # <INSTALLED_ADDONS>  # Warning: text inside the INSTALLED_ADDONS tags is auto-generated. Manual changes will be overwritten.
    "aldryn-addons",
    "aldryn-django",
    "aldryn-sso",
    "aldryn-celery",
    "aldryn-wagtail",
    # </INSTALLED_ADDONS>
]


aldryn_addons.settings.load(locals())


# all django settings can be altered here
USE_I18N = True
INSTALLED_APPS.extend(  # noqa: F821
    [
        "social_django",
        #    'wagtail.contrib.modeladmin',  # Don't repeat if it's there already
        "wagtail.contrib.settings",
        "widget_tweaks",
        "wagtailmenus",
        "wagtailimportexport",
        # add your project specific apps here
        "wagtail.contrib.styleguide",
        "home",
    ]
)
TEMPLATES[0]["OPTIONS"]["context_processors"].append(  # noqa: F821
    "wagtail.contrib.settings.context_processors.settings"
)
TEMPLATES[0]["OPTIONS"]["context_processors"].append(  # noqa: F821
    "wagtailmenus.context_processors.wagtailmenus"
)

WAGTAILMENUS_PAGE_FIELD_FOR_MENU_ITEM_TEXT = "translated_title"
WAGTAILMENUS_FLAT_MENU_ITEMS_RELATED_NAME = "custom_menu_items"

SOCIAL_AUTH_TRAILING_SLASH = False  # Remove end slash from routes
SOCIAL_AUTH_AUTH0_DOMAIN = "bps.eu.auth0.com"
SOCIAL_AUTH_AUTH0_KEY = "HHUhK5IHv30LICMVRRTNPwmeNHIQq8bP"
SOCIAL_AUTH_AUTH0_SECRET = "TVDIdrF5Me-LqQwAJMN7SnleTTlm2aJfEetUKKYvde8gvto1AZbH4sIAnd4xebwQ"

# TBD: Separate local env and use local BPS API during the development
BPS_API_ROOT = "https://admin.budapestschool.org/api/m2m-v1"

SOCIAL_AUTH_AUTH0_SCOPE = ["openid", "profile", "email"]

AUTHENTICATION_BACKENDS = {
    "auth0login.auth0backend.Auth0",
    "django.contrib.auth.backends.ModelBackend",
}

LOGIN_URL = "/login/auth0"
LOGIN_REDIRECT_URL = "/admin"
LOGOUT_REDIRECT_URL = "/"

WAGTAIL_FRONTEND_LOGIN_URL = LOGIN_URL
SOCIAL_AUTH_PIPELINE = (
    "social_core.pipeline.social_auth.social_details",
    "social_core.pipeline.social_auth.social_uid",
    "social_core.pipeline.social_auth.auth_allowed",
    "social_core.pipeline.social_auth.social_user",
    "social_core.pipeline.user.get_username",
    "social_core.pipeline.social_auth.associate_by_email",  # <--- enable this one
    "social_core.pipeline.user.create_user",
    "social_core.pipeline.social_auth.associate_user",
    "social_core.pipeline.social_auth.load_extra_data",
    "social_core.pipeline.user.user_details",
    "auth0login.social_auth_pipeline.make_bps_users_admin",
    # "auth0login.social_auth_pipeline.sync_users_with_bps",
)

SMTP_PORT = os.environ.get("SMTP_PORT")
SMTP_PASSWORD = os.environ.get("SMTP_PASSWORD")
SMTP_SERVER = os.environ.get("SMTP_SERVER")
SMTP_USERNAME = os.environ.get("SMTP_USERNAME")
SMTP_USE_TLS = True

BASE_URL = "http://new.budapestschool.org"
